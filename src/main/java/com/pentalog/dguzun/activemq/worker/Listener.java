package com.pentalog.dguzun.activemq.worker;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

public class Listener implements MessageListener {

	public void onMessage(Message message) {
		try {
			MapMessage map = (MapMessage) message;
			String destination = map.getString("destination");
			String name = map.getString("name");
			int age = map.getInt("age");
			boolean isAdmin = map.getBoolean("isAdmin");
			System.out.println(destination + "\tName:" + name + "\tAge:" + age
					+ "\tIs admin:" + isAdmin);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
